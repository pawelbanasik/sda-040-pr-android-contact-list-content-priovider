package com.example.rent.sda_040_pr_android_contact_list;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Telephony;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.LinkedList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MessagesActivity extends AppCompatActivity {

    @BindView(R.id.list_view_messages)
    protected ListView messagesList;

    private ArrayAdapter<String> messagesAdapter;
    private int contact_id;
    private String contact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);
        ButterKnife.bind(this);

        messagesAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                new LinkedList<String>());

        messagesList.setAdapter(messagesAdapter);

        if (getIntent().hasExtra("number")){
            contact = getIntent().getStringExtra("number");


        }



    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshMessages();
    }

    private void refreshMessages(){
        messagesAdapter.clear();
        ContentResolver resolver = getContentResolver();
        Uri messagesUri = Telephony.Sms.CONTENT_URI;
        Cursor cursor = resolver.query(messagesUri,
                null,
                Telephony.Sms.ADDRESS + "=?",
                new String[]{contact},
                null);

        int conversationColumnId = cursor.getColumnIndex(Telephony.Sms.BODY);
//        int phonesNumberColumnId = cursor.getColumnIndex(Telephony.Sms.ADDRESS);
        for (int i = 0; i < cursor.getCount(); i++) {
            cursor.moveToNext();
            String conversation = cursor.getString(conversationColumnId);
//            String phone = cursor.getString(phonesNumberColumnId);

            messagesAdapter.add(conversation);
        }
    }

}

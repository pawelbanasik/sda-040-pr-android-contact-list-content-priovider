package com.example.rent.sda_040_pr_android_contact_list;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;

public class MainActivity extends AppCompatActivity {

    private boolean permissionsChecked = false;

    @BindView(R.id.list_view)
    protected ListView listView;
    private ArrayAdapter<Contact> contactsAdapter;

    @OnItemClick(R.id.list_view)
    protected void listItemClick(int position) {
        Contact clickedContact = contactsAdapter.getItem(position);

        String numberToPass = clickedContact.getNumbers().get(0);
        Intent i = new Intent(this, MessagesActivity.class);
        i.putExtra("number", numberToPass);
        startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        contactsAdapter = new ArrayAdapter<Contact>(this, android.R.layout.simple_list_item_1, new ArrayList<Contact>());
        listView.setAdapter(contactsAdapter);
        isPermissionGranted();


    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshContacts();
    }

    public boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.READ_CONTACTS)
                    == PackageManager.PERMISSION_GRANTED &&
                    checkSelfPermission(Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED) {
                permissionsChecked = true;
                Log.v(getClass().getName(), "Permission is granted");
                return true;
            } else {
                Log.v(getClass().getName(), "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CONTACTS, Manifest.permission.READ_SMS}, 1);
                return false;
            }
        } else {
            Log.v(getClass().getName(), "Permission is granted");
            return true;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            permissionsChecked = true;
            refreshContacts();
        }
    }

    private void refreshContacts() {
        if (!permissionsChecked) {
            return;
        }

        contactsAdapter.clear();
        Uri contactsURI = ContactsContract.Contacts.CONTENT_URI;
        Uri phoneNumbersUri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;

        ContentResolver resolver = getContentResolver();
        Cursor contactsCursor = resolver.query(contactsURI, null, null, null, null);
        int contactNameColumnId = contactsCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
        int contactContactHasNumberColumnId = contactsCursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER);
        int contactIdColumnId = contactsCursor.getColumnIndex(ContactsContract.Contacts._ID);


        String phoneContactIdColumn = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;

        for (int i = 0; i < contactsCursor.getCount(); i++) {
            contactsCursor.moveToNext();

            int contactId = contactsCursor.getInt(contactIdColumnId);
            String contactName = contactsCursor.getString(contactNameColumnId);
            int contactPhoneNumber = contactsCursor.getInt(contactContactHasNumberColumnId);

            if (contactPhoneNumber == 0) {
                contactName += " - no number";
            }


            Contact contact = new Contact(contactId, contactName);

            Cursor phoneNumberCursor = resolver.query(phoneNumbersUri,
                    null,
                    phoneContactIdColumn + "=?",
                    new String[]{String.valueOf(contactId)},
                    null);

            int phoneNumberColumnId = phoneNumberCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

            for (int j = 0; j < phoneNumberCursor.getCount(); j++) {
                phoneNumberCursor.moveToNext();

                String phoneNumber = phoneNumberCursor.getString(phoneNumberColumnId);
                contactName += " " + phoneNumber;
                contact.getNumbers().add(phoneNumber);
            }

            phoneNumberCursor.close();
            contactsAdapter.add(contact);


        }
        contactsCursor.close();
    }

}

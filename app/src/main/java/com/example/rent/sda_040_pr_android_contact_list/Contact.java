package com.example.rent.sda_040_pr_android_contact_list;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by RENT on 2017-06-07.
 */

public class Contact {
    private int id;
    private String name;
    private List<String> numbers;

    public Contact(int id, String name) {
        this.numbers = new LinkedList<>();
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getNumbers() {
        return numbers;
    }

    public void setNumbers(List<String> numbers) {
        this.numbers = numbers;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", numbers=" + numbers +
                '}';
    }
}
